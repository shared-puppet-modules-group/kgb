# @summary configure a kgb-bot client
#
# @param repo_id
#   Repository id.
# @param password
#   Repository password.
# @param servers
#   Server you want the client to connect to.
# @param priv
#   If the repository should be marked as "private", that is, not
#   relayed to the common channel.
# @param export_tag
#   If set, export a tagged configuration to be collected on the
#   server.
#
define kgb::client(
  String $repo_id,
  Enum['present', 'absent'] $ensure = 'present',
  Optional[String] $password = undef,
  Optional[Array[Hash[String,String]]] $servers = undef,
  Optional[Boolean] $priv = false,
  Optional[Hash] $extra_config = undef,
  Optional[String] $export_tag = undef,
) {
    file { $name:
      ensure  => $ensure,
      mode    => '0400',
      content => epp('kgb/kgb-client.conf.epp', {
        repo_id  => $repo_id,
        password => $password,
        servers  => $servers,
        extra    => $extra_config,
      }),
    }
    if $ensure == 'present' and $export_tag {
      @@file { "/etc/kgb-bot/kgb.conf.d/client-repo-${repo_id}.conf":
        mode    => '0400',
        owner   => 'Debian-kgb',
        tag     => $export_tag,
        content => @("EOF")
        repositories:
          ${repo_id}:
            private: ${priv}
            password: |-
              ${password}
        |EOF
      }
    }
}
