# @summary install and configure a kgb-bot service
#
# Further configuration can be placed in /etc/kgb/kgb.conf.d/ and will be
# included when the bot restarts. If you "notify" this profile, it should
# restart the 'kgb' service for you.
#
# @see https://manpages.debian.org/buster/kgb-bot/kgb.conf.5p.en.html kgb.conf(5)
#
# @todo We should define more precise data type aliases in ../types/ -- it will
#   help to avoid bullshit input, but it will also help to guide users in what
#   values are valid.
#
# @todo just an idea: should we replace $service_running by a boolean value,
#   maybe named $enable_service ? it would make it easier later to transform to
#   either running/stopped or 0/1, and it would be easier for users to decide
#   what value is needed.
#
# @param ensure
#   Set to absent to remove traces of this class.
# @param service_ensure
#   Set to 'running' to have the service start or to 'stopped' to have it be
#   disabled.
# @param listen_address
#   which address should the bot listen on (default: 127.0.0.1)
# @param listen_port
#   which port should the bot run on (default: 5391)
# @param admins
#   An array of IRC nicks for people who should be able to manage the bot.
#   kgb-bot currently only has a command to display it's version. Note: this
#   doesn't seem to work super reliably.
# @param channels
#   An array of hashes to for the channels to configure. Eg,.
#   [{'name'        => '#mychannel', 'network' => 'freenode', 'repos' => [...]}]
#   Note that the network and repos are referenced by name to those defined in
#   the parameters networks and repositories.
# @param networks
#   A Hash[String, Hash] of networks that the bot should connect to indexed by
#   the reference name.
# @param repositories
#   A Hash[String, Hash] of repositories that should be defined in the
#   configuration. Eg.
#   {'control-repo' => {'password' => 'xxx', 'private' => yes'}}
# @param realize_tag
#   If set, realize the File resources tagged with the given tag. Used
#   to collect repository configuration exported by the clients.
class kgb (
  Enum[present, absent] $ensure = 'present',
  Enum[running, stopped] $service_ensure = 'running',
  String $listen_address = '127.0.0.1',
  Integer $listen_port = 5391,
  Array[String[1]] $admins = [],
  Array[Hash] $channels = [],
  Hash[String, Hash] $networks = {},
  Optional[Hash[String, Hash]] $repositories = undef,
  Optional[String] $realize_tag = undef,
) {
  ensure_packages(['kgb-bot', 'kgb-client'], {ensure => $ensure})
  $vars = {
    'listen_address' => $listen_address,
    'listen_port'    => $listen_port,
    'admins'         => $admins,
    'channels'       => $channels,
    'networks'       => $networks,
    'repositories'   => $repositories,
  }
  file { '/etc/kgb-bot/kgb.conf':
    ensure  => $ensure,
    content => epp('kgb/kgb.conf.epp', $vars),
    owner   => 'root',
    group   => 'Debian-kgb',
    mode    => '0440',
    require => Package['kgb-bot'],
    notify  => Service['kgb-bot'],
  }
  service { 'kgb-bot':
    ensure  => $service_ensure,
    require => Package['kgb-bot'],
    # Disable start on boot if we ensure 'stopped'.
    enable  => $service_ensure == 'running',
  }
  if $ensure {
    $enable_value = $service_ensure ? {
      'running' => '1',
      default => '0',
    }
    file_line { 'kgb_bot_enable':
      ensure => present,
      path   => '/etc/default/kgb-bot',
      line   => "BOT_ENABLED=${enable_value}",
      match  => 'BOT_ENABLED=',
      notify => Service['kgb-bot'],
    }
    if $realize_tag {
      File <<| tag == $realize_tag |>> {
        notify => Service['kgb-bot'],
      }
    }
  }
}
